import React from 'react'
import Link from 'next/link'

import { 
  Row,
  Col,
  ListGroup, 
  ListGroupItem, 
  Badge 
} from 'reactstrap'

interface BookmarksListProps {
  bookmarks: any[]
}

interface BookmarkListItemProps {
  id?: string,
  name?: string,
  url?: string
}

function BookmarkListItem(props: BookmarkListItemProps) {
  const {
    id,
    url,
    name
  } = props

  return (
    <ListGroupItem>
    <Row>
      <Col xs="9">
        <Link href={url}><a>{name || url}</a></Link>
      </Col>
      <Col>
        {id && <Link href={`/edit/${id}`}><a>Edit</a></Link>}
      </Col>
      <Col></Col>
    </Row>
    </ListGroupItem>
  )
}

export default BookmarkListItem