import React from 'react'

import { useLocalStorage } from '../hooks/useLocalStorage'

interface AuthContextValue {
  accessToken?: string,
  setAccessToken: (accessToken: string) => any
}

export const AuthContext = React.createContext<AuthContextValue>({
  setAccessToken: (token: string) => { throw new Error("Unimplemented") }
})

export const AuthProvider = (props) => {
  // const [accessToken, setAccessToken] = React.useState<string | undefined>()
  const [accessToken, setAccessToken] = useLocalStorage("accessToken", undefined)

  const value = {
    accessToken,
    setAccessToken
  }

  return <AuthContext.Provider value={value} {...props} />
}