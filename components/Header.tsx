import React from 'react'
import Link from 'next/link'

import { useAuth } from '../hooks/useAuth'

import { 
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem
} from 'reactstrap'
 
function Header() {
  const auth = useAuth()

  const [isOpen, setIsOpen] = React.useState(false)

  function toggle() {
    setIsOpen(!isOpen)
  }

  return (
    <Navbar color="light" light expand="md">
      <NavbarBrand href="/">My Bookmarks</NavbarBrand>
      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={isOpen} navbar>
        <Nav className="ml-auto" navbar>
          {!auth.accessToken 
            ? (
              <NavItem>
                <Link href="/login">Log In</Link>
              </NavItem>
            )
            : (
              "Username"
            )
          }
        </Nav>
      </Collapse>

        {/* {!auth.accessToken 
          ? <Link href="/login">
              <a>Login</a>
            </Link> 
          : <p>Logged In</p>} */}
    </Navbar>
  )
}

export default Header