import React from 'react'
import Router from 'next/router'
import { 
  Alert,
  Container, 
  Row, 
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Button,
  Spinner
} from 'reactstrap'

import { useAuth } from '../hooks/useAuth'

interface LoginProps {
}

enum FetchMethod {
  Post = "post"
}

const Login = (props: LoginProps) => {
  const [email, setEmail] = React.useState<string>()
  const [password, setPassword] = React.useState<string>()
  const [error, setError] = React.useState()

  const [isLoading, setIsLoading] = React.useState(false)

  const auth = useAuth()

  async function login(event) {
    event.preventDefault()

    setIsLoading(true)
    setError(undefined)

    try {
      const response = await fetch('http://localhost:3000/api/login', {
        method: FetchMethod.Post,
        headers: {
          "Content-Type": "application/json"
        },
        mode: "cors",
        body: JSON.stringify({ username: email, password })
      })

      const body = await response.json()

      if (!response.ok) {
        throw new Error(body.error)
      }

      const { access_token: accessToken } = body

      if (!accessToken) {
        throw new Error("No access_token in body")
      }

      auth.setAccessToken(accessToken)

      Router.push('/')
    } catch (error) {
      setError(error)
    } finally {
      setIsLoading(false)
    }
  }

  return (
    <Container>
      <h1>My Bookmarks</h1>
      {error && <Alert color="danger">{error.message}</Alert>}
      <Form onSubmit={login}>
        <FormGroup>
          <Label for="email">Email</Label>
          <Input type="email" id="email" name="email" onChange={event => setEmail(event.target.value)} value={email}/>
        </FormGroup>
        <FormGroup>
          <Label for="password">Password</Label>
          <Input type="password" onChange={event => setPassword(event.target.value)} value={password}/>
        </FormGroup>
        <Button color="primary">{isLoading ? <Spinner size="sm"/> : "Submit"}</Button>
      </Form>
    </Container>
  )
}

export default Login