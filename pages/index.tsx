import React from 'react'
import Link from 'next/link'
import Head from 'next/head'

import {
  ListGroup
} from 'reactstrap'

import BookmarkListItem from '../components/BookmarkListItem'

import { useAuth } from '../hooks/useAuth'

import 'bootstrap/dist/css/bootstrap.min.css'

function useFetch() {
}

const Home = () => {
  const auth = useAuth()

  const [bookmarks, setBookmarks] = React.useState([])

  React.useEffect(() => {
    fetch('http://localhost:3000/api/bookmarks', {
      method: 'GET',
      headers: {
        "Authorize": `Bearer ${auth.accessToken}`,
        "Content-Type": `application/json`
      }
    })
    .then(response => response.json())
    .then(setBookmarks)
  }, [])

  return (
    <div>
      <Head>
        <title>Home</title>
      </Head>
      <div>
        <ListGroup>
          <BookmarkListItem url="/new" name="New Bookmark" />
          {bookmarks.map(bm => <BookmarkListItem {...bm} />)}
        </ListGroup>
      </div>
    </div>
  )
}

export default Home
