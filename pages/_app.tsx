import React from 'react'
import App from 'next/app'

import 'bootstrap/dist/css/bootstrap.min.css'

import { AuthProvider } from '../components/AuthProvider'
import Header from '../components/Header'

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props

    return (
      <AuthProvider>
        <div>
          <Header />
          <Component {...pageProps } />
        </div>
      </AuthProvider>
    )
  }
}

export default MyApp
