import React from 'react'

import { 
  Button,
  Container,
  Form,
  FormGroup,
  Input,
  Label
} from 'reactstrap'

import Router from 'next/router';

import { useAuth } from '../hooks/useAuth'

function NewBookmark() {
  const [name, setName] = React.useState()
  const [url, setUrl] = React.useState()
  const [description, setDescription] = React.useState()

  const auth = useAuth()

  async function saveBookmark(event) {
    event.preventDefault()

    try {
      await fetch("http://localhost:3000/api/bookmarks", {
        method: 'POST',
        headers: {
          "Authorization": `Bearer ${auth.accessToken}`,
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          name,
          url,
          description
        })
      })

      Router.push('/')
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <Container>
      <h1>New Bookmark</h1>
      <Form onSubmit={saveBookmark}>
        <FormGroup>
          <Label for="name">Name</Label>
          <Input id="name" name="name" value={name} onChange={event => setName(event.target.value)}/>
        </FormGroup>
        <FormGroup>
          <Label for="url">Url</Label>
          <Input id="url" name="url" type="url" value={url} onChange={event => setUrl(event.target.value)} />
        </FormGroup>
        <FormGroup>
          <Label for="desc">Description</Label>
          <Input type="textarea" id="desc" value={description} onChange={event => setDescription(event.target.value)} />
        </FormGroup>
        <Button>Save</Button>
      </Form>
    </Container>
  )
}

export default NewBookmark