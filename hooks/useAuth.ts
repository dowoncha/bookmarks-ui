import React from 'react'
import { AuthContext } from '../components/AuthProvider'

export function useAuth() {
  const authContext = React.useContext(AuthContext)

  if (!authContext) {
    throw new Error("useAuth must be used within an AuthContext Provider")
  }

  return authContext
}